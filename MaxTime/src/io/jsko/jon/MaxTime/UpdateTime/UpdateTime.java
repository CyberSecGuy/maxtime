package io.jsko.jon.MaxTime.UpdateTime;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.World;

import io.jsko.jon.MaxTime.Main;

public class UpdateTime {
	public static Main plugin;

	String[] args;

	public UpdateTime(Main instance) {
		plugin = instance;
	}

	public static void runTimeAdjustment() {

		

		Thread thread = new Thread() {
			public void run() {
				DateFormat dg = new SimpleDateFormat("HHmm");
				Date date = new Date();
				String rdat = dg.format(date) + "0";
				int time = Integer.parseInt(rdat);
				time -= 6000;
				if (time < 0) {
					time += 24000;
				}

				if (Main.debugMode) {
					Bukkit.getServer().broadcastMessage("runTimeAdjustment time:" + time);
				}
				
				for (World w : Bukkit.getWorlds()) {
					if (Main.disableWorlds.contains(w.getName())) {
						w.setTime(time);
					}
				}
			}
		};
		thread.start();
	}

	public static void scheduler(Main main) {
		Main.taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(main, new Runnable() {
			public void run() {
				runTimeAdjustment();
			}
		}, 20, Main.updateInterval * 20);
	}

//	public static void scheduler(Main main) {
//		new BukkitRunnable() {
//			@Override
//			public void run() {
//				runTimeAdjustment();
//			}
//		}.runTaskTimer(main, 20, Main.updateInterval * 20);
//	}
}
