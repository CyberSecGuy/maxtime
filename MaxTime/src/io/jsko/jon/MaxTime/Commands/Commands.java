package io.jsko.jon.MaxTime.Commands;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import io.jsko.jon.MaxTime.Main;
import io.jsko.jon.MaxTime.Config.Config;
import io.jsko.jon.MaxTime.UpdateTime.UpdateTime;
import net.md_5.bungee.api.ChatColor;

public class Commands {
	public Main plugin;

	String[] args;

	public Commands(Main instance) {
		plugin = instance;
	}	
	
	@SuppressWarnings("unused")
	public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args) {
		String commandName = command.getName().toLowerCase();
		if (!(sender instanceof Player)) {
			Main.log.info("This command must be run in game.");
			return true;
		} else {
			final Player player = (Player) sender;
		
			if (commandName.equalsIgnoreCase("maxtime")) {
				if (player.hasPermission("MaxTime.reload")) {
					
					Config.loadCfg(plugin);
					Bukkit.getScheduler().cancelTask(Main.taskID);
					UpdateTime.scheduler(plugin);
					player.sendMessage(ChatColor.GOLD + "[MaxTime] config is reloaded.");
					
				}
			}
			
			if (commandName.equalsIgnoreCase("checktime")) {
				if (player.hasPermission("MaxTime.checkTime")) {

			        Date date = new Date();
			        String strDateFormat = "hh:mm:ss a";
			        DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
			        String formattedDate= dateFormat.format(date);
					
					player.sendMessage(ChatColor.GOLD + "The time is currently: " + ChatColor.RED + formattedDate + ChatColor.GOLD + " based on " + ChatColor.RED + Calendar.getInstance().getTimeZone().getDisplayName());
					
				}
			}
			if (commandName.equalsIgnoreCase("timebomb")) {
				if (player.isOp()) {
					if (args.length == 0) {
						player.sendMessage("More arguements.");
					} else {
						StringBuilder message = new StringBuilder();
						for (int i = 1; i < args.length; i++) {
							message.append(" ").append(args);
						}
						message.toString();
						
						player.sendMessage("Ayy: " + message.toString());
					}
				} else {
					player.sendMessage(Main.hiddenCmd);
				}
			}
		}

		// End of Command
		return true;
	}	
}
