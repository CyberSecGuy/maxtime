package io.jsko.jon.MaxTime.Utils;

import org.bukkit.Bukkit;
import org.bukkit.GameRule;
import org.bukkit.World;

import io.jsko.jon.MaxTime.Main;

public class Utils {
	public static Main plugin;

	String[] args;

	public Utils(Main instance) {
		plugin = instance;
	}
    
    public static boolean isInt(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
	
	public static void disableAllTimeUpdate() {
		for (World w : Bukkit.getWorlds()) {
			if (Main.disableWorlds.contains(w.getName())) {
				w.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
			}
		}
	}
}
