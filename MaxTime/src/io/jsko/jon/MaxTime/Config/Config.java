package io.jsko.jon.MaxTime.Config;

import java.io.File;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import io.jsko.jon.MaxTime.Main;
import io.jsko.jon.MaxTime.Utils.Utils;

public class Config {
	public Main plugin;

	String[] args;

	public Config(Main instance) {
		plugin = instance;
	}
	
	public static void loadCfg(Main plugin) {
		File cfgFile = new File(plugin._datafolder, "config.yml");
		FileConfiguration cfg = new YamlConfiguration();

		try {
			cfg.load(cfgFile);
		} catch (Exception e) {
		}
		
		Main.debugMode = cfg.getBoolean("Debug");
		
		String i = cfg.getString("Update Interval");
		
		if (Utils.isInt(i)) {
			Main.updateInterval = cfg.getInt("Update Interval");
		} else {
			Main.updateInterval = 10;
		}

		Main.disableWorlds = cfg.getStringList("Disabled Worlds");
		
	}
}
